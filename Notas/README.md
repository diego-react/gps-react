# Tareas app

App para guardar y administrar las tareas que tienes por realizar.
## Install

```npm
npm install
```

# Usage

```bash
npm start
```
# Contributing

Proyecto creado a modo laboratorio para la clase de GPS
# License

GPS-REACT is released under the [MIT License](https://opensource.org/licenses/MIT)