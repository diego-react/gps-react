import React, { useEffect, useState } from 'react';
import './estilos.css';
import Plus from '@mui/icons-material/ControlPoint';
import Modal from 'react-modal';
import axios from 'axios';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

function Agregar(props) {
  let subtitle;
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    subtitle.style.color = '#f00';
  }

  function closeModal() {
    setIsOpen(false);
  }

  const [newTareaValue, setNewTareaValue] = useState('');
  const [newDateValue, setNewDateValue] = useState('');
  const [newTimeValue, setNewTimeValue] = useState('');

  const onChange = (event) => {
    setNewTareaValue(event.target.value);
  };
  const onChange2 = (event) => {
    setNewDateValue(event.target.value);
  };
  const onChange3 = (event) => {
    setNewTimeValue(event.target.value);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    props.crear(newTareaValue);
    axios
      //POST
      .post('api', {
        newDateValue,
        newTareaValue,
        newTimeValue,
      })
      .then((res) => console.log('Petición Post', res))
      .catch((err) => console.log(err));
  };

  return (
    <>
      <div className='parent'>
        <button onClick={openModal}>
          <Plus />
        </button>
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel='Example Modal'
        >
          <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Agragar Tarea</h2>
          <form onSubmit={onSubmit}>
            <input value={newTareaValue} onChange={onChange} />
            <input type='date' name='trip-start' onChange={onChange2} />
            <input type='time' onChange={onChange3} />
            <br />
            <button onClick={closeModal}>close</button>
            <button type='submit'>crear</button>
          </form>
        </Modal>
      </div>
    </>
  );
}
export { Agregar };
