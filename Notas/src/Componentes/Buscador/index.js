import React from "react";
import './estilos.css';

function Buscador({ buscandoValor, setBuscandoValor }) {

    const buscando = (event) => {
        console.log(event.target.value);
        setBuscandoValor(event.target.value)
    }

    return (
        <>
        <input placeholder="Buscar Tareas" type="text" className="text" onChange={buscando}></input>
        </>
    )
}
export { Buscador }