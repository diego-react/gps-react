import React from "react";
import './estilos.css';

function Contador({ total, completadas }) {
    return (
        <h1>Has completado: {completadas} tareas de: {total}</h1>
    )
}
export { Contador }