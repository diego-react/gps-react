import React from "react";
import Check from "@mui/icons-material/Check";
import Delete from "@mui/icons-material/Delete";
import { Actualizar } from './../Actualizar/index';
import './estilos.css';


function Item(props){
    
    return(
            <>
            <tr className="TodoItem">
            <td><button className="button button1" onClick={props.onComplete}><Check /></button></td>
            <td className={`TodoItem-p ${props.completar && 'TodoItem-p--complete'}`}>{props.text}</td>
            <td><button className="button button2" onClick={props.onDelete}><Delete /></button></td>
            <td><Actualizar comple={props.completar} idTarea={props.onUpdate} date={props.date} time={props.time}/></td>
            </tr>
            </>
    )
}
export {Item}