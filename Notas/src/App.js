import './App.css';
import React, { useEffect, useState } from 'react';
import { Titulo } from './Componentes/Titulo/index';
import { Buscador } from './Componentes/Buscador/index';
import { Contador } from './Componentes/Contador/index';
import { Lista } from './Componentes/Lista/index';
import { Agregar } from './Componentes/Agregar/index';
import { Item } from './Componentes/Item/index';
import axios from 'axios';

function App() {

  const notificarme = () => {
    if (!window.Notification) {
      console.log('Este navegador soporta notificaciones');
      return
    }
    if (Notification.permission === 'granted') {
      
    } else if (Notification.permission !== 'denied' || Notification.permission === 'default'){
      Notification.requestPermission((permission) => {
        console.log(permission);
        if (permission === 'granted') {
          new Notification('Welcome to SquareNote');
        }
      })
    }
  }

  notificarme();

  const [tareas, setTareas] = useState([]);

  //Petición get
  const obtenerTareas = async () => {
    const url = 'api';
    const result = await axios.get(url);
    setTareas(result.data);
  };

  // Crear useEffect
  useEffect(() => {
    obtenerTareas();
  }, [tareas])


  const TareasCompletadas = tareas.filter((tarea) => !!tarea.completed).length;

  const TareasTotal = tareas.length;

  const [buscandoValor, setBuscandoValor] = useState('');

  let buscarTareas = [];

  if (!buscandoValor.length >= 1) {
    buscarTareas = tareas;
  } else {
    buscarTareas = tareas.filter((tarea) => {
      const tareaText = tarea.text.toLowerCase();
      const buscarText = buscandoValor.toLowerCase();

      return tareaText.includes(buscarText);
    });
  }

  const saveTareas = (nuevasTareas) => {
    const stringTareas = JSON.stringify(nuevasTareas);
    localStorage.setItem('tareas-1', stringTareas);
    setTareas(nuevasTareas);
  };

  const addTarea = (tarea) => {
    const newTarea = [...tareas];
    newTarea.push({
      completed: false,
      tarea,
    });
    saveTareas(newTarea);
  };
  
  //Completar tareas
  const completarTarea = (id, text, completed, date, time, e) => {
    let comple;

    if (completed !== true) {
      comple = true;
    } else {
      comple = false
    }

    const actualizarTarea = {
      text: text,
      completed: comple,
      date: date,
      time: time
    }
    console.log(actualizarTarea);
    e.preventDefault();
    axios
      //PUT
      .post(`api/${id}`, {
        actualizarTarea
      })
        .then((res) => console.log('Petición Put', res))
          .catch((err) => console.log(err));
  };


  const postDelete = (id, e) => {
    e.preventDefault();
    axios.delete(`api/${id}`)
      .then(res => console.log('Delete', res))
        .catch(err => console.log(err))
  }

  return (
    <>
      <Titulo />
      <Contador total={TareasTotal} completadas={TareasCompletadas} />
      <Buscador
        buscandoValor={buscandoValor}
        setBuscandoValor={setBuscandoValor}
      />
      <Agregar crear={addTarea} />
      <Lista>
        {buscarTareas.map((tarea) => (
          <Item
            text={tarea.text}
            key={tarea.text}
            completar={tarea.completed}
            date={tarea.date}
            time={tarea.time}
            onComplete={(e) => completarTarea(tarea.id, tarea.text, tarea.completed, tarea.data, tarea.time, e)}
            onUpdate={tarea.id}
            onDelete={(e) => postDelete(tarea.id, e)}
          />
        ))}
      </Lista>
    </>
  );
}

export default App;
